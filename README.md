# TCC CODE

## Overview

**[tcccode] [1]** is a for *TCC* code developed by [@wagnersq_] from ARL course at UFLA hosted in [Bitbucket Site] [2]

Reason
======
I'm using this code to show the software developing by Delivery Integration methodology.

Initial Commands
----------------
```sh
$ git config --global user.name "Wagner da Silva Queiroz"
$ git config --global user.email "wagnersq@gmail.com"
$ mkdir code
$ cd code/
$ git init
$ git remote add origin git@bitbucket.org:wagnersq/tcccode.git
$ ssh-keygen -t rsa -C "wagnersq@gmail.com"
$ more ~/.ssh/known_hosts
$ xclip -sel clip < ~/.ssh/id_rsa.pub # copy into git repo settings
$ touch README.md
$ echo "README.md file content" > README.md
$ touch ~/.ssh/config
$ echo "Host bitbucket.org
  IdentityFile ~/.ssh/id_rsa" > ~/.ssh/config
$ chmod 700 ~/.ssh
$ chmod 600 ~/.ssh/config
$ ssh -T git@bitbucket.org # To test connection with bitbucket and load the rsa key
$ git add .
$ git commit -m "Specific commit message"
$ git push -u origin master
$ git status
```

Version
-------

1.0

Tech
-----------

TCC CODE uses a number of open source projects to work properly:

* [Bitbucket] -  is a web-based hosting service for projects that use either the Mercurial or Git revision control systems.
* [Eclipse] - IDE Java application
* [Java] - a high-level language and software-only platform
* [Linux Educacional] - Educational Linux Distribution of Brazilian Federal Government
* [Texlipse] - TeXlipse is a plugin that adds Latex support to the Eclipse IDE

License
----

No license

[1]:https://bitbucket.org/wagnersq/tcccode
[2]:http://bitbucket.org
[Bitbucket]: http://bitbucket.org
[Eclipse]:http://eclipse.org
[Java]:http://docs.oracle.com/javase/
[Linux Educacional]:http://linuxeducacional.c3sl.ufpr.br/
[Texlipse]:http://texlipse.sourceforge.net/
[@wagnersq_]:http://twitter.com/wagnersq_


