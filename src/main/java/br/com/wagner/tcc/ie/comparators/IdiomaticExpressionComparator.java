package br.com.wagner.tcc.ie.comparators;

import java.util.Comparator;

import br.com.wagner.tcc.ie.models.IdiomaticExpression;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public final class IdiomaticExpressionComparator implements Comparator<IdiomaticExpression> {

	@Override
	public int compare(final IdiomaticExpression ie1, final IdiomaticExpression ie2) {

		if (null == ie1)
			return -1;

		if (null == ie2)
			return 1;

		int expression = ie1.getExpression().compareTo(ie2.getExpression());

		int translate = (expression == 0) ? ie1.getTranslate().compareTo(ie2.getTranslate()) : expression;

		int from = (translate == 0) ? ie1.getFrom().compareTo(ie2.getFrom()) : translate;

		int to = (translate == 0) ? ie1.getTo().compareTo(ie2.getTo()) : from;

		return to;
	}

	@Override
	public String toString() {

		return this.getClass().getCanonicalName();
	}
}