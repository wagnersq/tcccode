package br.com.wagner.tcc.ie.models;

import br.com.wagner.tcc.ie.exceptions.InvalidAttributeStateException;
import br.com.wagner.tcc.ie.utils.Proposition;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public final class Word {

	private final String word;

	/**
	 * Custom constructor
	 * 
	 * @param word
	 */
	public Word(final String word) throws InvalidAttributeStateException {

		this.word = this.checkWord(word);
	}

	/**
	 * Verify the valid state of word
	 * 
	 * @param word
	 * @return word
	 * @throws InvalidAttributeStateException
	 */
	private String checkWord(final String word) throws InvalidAttributeStateException {

		Proposition.checkValue(word);

		return word;
	}

	/**
	 * Return the word
	 * 
	 * @return word
	 */
	public String getWord() {

		return this.word;
	}

	@Override
	public String toString() {

		return this.getClass().getCanonicalName();
	}

}