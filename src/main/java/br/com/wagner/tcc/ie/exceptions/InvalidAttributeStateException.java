package br.com.wagner.tcc.ie.exceptions;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public class InvalidAttributeStateException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Custom constructor
	 */
	public InvalidAttributeStateException() {
		super();
	}

	/**
	 * Custom constructor
	 * 
	 * @param exception
	 */
	public InvalidAttributeStateException(final Exception exception) {
		super(exception);
	}

	@Override
	public String toString() {

		return this.getClass().getCanonicalName();
	}

}