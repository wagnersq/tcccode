package br.com.wagner.tcc.ie.exceptions;

import java.io.IOException;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public class PageNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Custom constructor
	 * 
	 * @param ioe
	 */
	public PageNotFoundException(final IOException ioe) {

		super(ioe);
	}

	@Override
	public String toString() {

		return this.getClass().getCanonicalName();
	}

}