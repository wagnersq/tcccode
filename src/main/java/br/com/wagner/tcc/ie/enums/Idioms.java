/**
 * 
 */
package br.com.wagner.tcc.ie.enums;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public enum Idioms {

	US_ENGLISH, FR_FRENCH, PT_BRAZILIAN;
}