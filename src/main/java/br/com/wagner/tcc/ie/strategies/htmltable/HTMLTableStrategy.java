package br.com.wagner.tcc.ie.strategies.htmltable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import br.com.wagner.tcc.ie.abstracts.AbstractRecoverStrategy;
import br.com.wagner.tcc.ie.enums.Idioms;
import br.com.wagner.tcc.ie.exceptions.SiteConnectionException;
import br.com.wagner.tcc.ie.interfaces.RecoverStrategy;
import br.com.wagner.tcc.ie.models.IdiomaticExpression;
import br.com.wagner.tcc.ie.models.Site;
import br.com.wagner.tcc.ie.models.Word;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public final class HTMLTableStrategy extends AbstractRecoverStrategy implements RecoverStrategy {

	private final BuilderHTMLFields htmlFields;

	private final Comparator<IdiomaticExpression> comparator;

	/**
	 * Custom constructor
	 * 
	 * @param site
	 * @param word
	 */
	public HTMLTableStrategy(final Site site, final Word word, final Idioms from, final Idioms to,
			final BuilderHTMLFields htmlFields, final Comparator<IdiomaticExpression> comparator) {

		super(site, word, from, to);

		this.htmlFields = htmlFields;

		this.comparator = comparator;
	}

	/**
	 * Return the html fields
	 * 
	 * @return the htmlFields
	 */
	public BuilderHTMLFields getHtmlFields() {

		return this.htmlFields;
	}

	/**
	 * Return the comparator
	 * 
	 * @return the comparator
	 */
	public Comparator<IdiomaticExpression> getComparator() {

		return this.comparator;
	}

	/**
	 * Returns a collection of idiomatic expressions
	 * 
	 * @return HttpURLConnection
	 * @throws SiteConnectionException
	 */
	public List<IdiomaticExpression> recoverIdiomaticExpressions() throws SiteConnectionException {

		final List<IdiomaticExpression> idiomaticExpressions = new ArrayList<IdiomaticExpression>(0);

		try {

			final String url = getSite().getStrURL();

			final Document doc = Jsoup.connect(url).get();

			for (final Element table : doc.select(htmlFields.getTable())) {

				for (final Element row : table.select(htmlFields.getTr())) {

					final Elements tds = row.select(htmlFields.getTd());

					final IdiomaticExpression ie = new IdiomaticExpression(super.getFrom(), super.getTo(), tds.get(0)
							.text(), tds.get(1).text());

					idiomaticExpressions.add(ie);
				}
			}

			Collections.sort(idiomaticExpressions, this.comparator);

		} catch (final IOException ioe) {

			throw new SiteConnectionException(ioe);
		}

		return idiomaticExpressions;
	}

	@Override
	public String toString() {

		return this.getClass().getCanonicalName();
	}
}