package br.com.wagner.tcc.ie;

import java.util.List;

import br.com.wagner.tcc.ie.comparators.IdiomaticExpressionComparator;
import br.com.wagner.tcc.ie.enums.Idioms;
import br.com.wagner.tcc.ie.models.IdiomaticExpression;
import br.com.wagner.tcc.ie.models.Site;
import br.com.wagner.tcc.ie.models.Word;
import br.com.wagner.tcc.ie.strategies.htmltable.BuilderHTMLFields;
import br.com.wagner.tcc.ie.strategies.htmltable.HTMLTableStrategy;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public final class App {

	public static void main(final String[] args) {

		String searchWord = null;

		if (null != args && args.length == 1 && !args[0].trim().isEmpty()) {

			searchWord = args[0].trim();

			new App().searchWord(searchWord);

		} else {

			System.err.println("Erro: digite uma palavra para a pesquisa.");
			System.err.println("Uso: java -jar PACKAGE_NAME-PACKAGE_VERSION.jar Word_To_Search");
		}

	}

	/**
	 * Interface method to search a idiomatic expression search.
	 */
	private void searchWord(final String searchWord) {

		try {

			final Word word = new Word(searchWord);

			final Site site = new Site("http://www.expressoesidiomaticas.com.br/idiomaticas/linguas_2_ingles/"
					+ word.getWord() + ".shtml");

			final BuilderHTMLFields htmlFields = new BuilderHTMLFields.Builder("table[id=tbcFrases]", "tr:gt(0)",
					"td:not([rowspan])").build();

			final HTMLTableStrategy htmlTableStrategy = new HTMLTableStrategy(site, word, Idioms.US_ENGLISH,
					Idioms.PT_BRAZILIAN, htmlFields, new IdiomaticExpressionComparator());

			final List<IdiomaticExpression> recoverIdiomaticExpressions = htmlTableStrategy
					.recoverIdiomaticExpressions();

			for (final IdiomaticExpression idiomaticExpression : recoverIdiomaticExpressions) {

				System.out
						.printf("%s : %s %n", idiomaticExpression.getExpression(), idiomaticExpression.getTranslate());
			}

			System.out.println("\nNúmero de expressões idiomaticas com --> " + searchWord + " <-- : " + recoverIdiomaticExpressions.size());
			
		} catch (Exception e) {
			
			System.err.println("ERRO: Não foi possível executar a consulta ou palavra não disponível.");

		}
	}
}

/**
 *
 * http://api.openweathermap.org/
 * 
 * http://www.ibm.com/developerworks/cloud/library/cl-watson-films-bluemix-app/
 * index.html
 * 
 * https://developer.yahoo.com/weather/
 * 
 * http://stackoverflow.com/questions/10771218/how-to-get-a-table-from-an-html-
 * page-using-java
 * 
 * 
 *
 */
