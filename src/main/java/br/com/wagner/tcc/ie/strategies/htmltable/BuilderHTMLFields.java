package br.com.wagner.tcc.ie.strategies.htmltable;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public final class BuilderHTMLFields {

	private final String table;

	private final String tr;

	private final String td;

	public static class Builder {

		private final String table;

		private final String tr;

		private final String td;

		public Builder(final String table, final String tr, final String td) {

			this.table = table;

			this.tr = tr;

			this.td = td;
		}

		public BuilderHTMLFields build() {

			return new BuilderHTMLFields(this);
		}
	}

	private BuilderHTMLFields(final Builder builder) {

		this.table = builder.table;

		this.tr = builder.tr;

		this.td = builder.td;
	}

	/**
	 * Return the table element name
	 * 
	 * @return the table
	 */
	public String getTable() {

		return this.table;
	}

	/**
	 * Return the tr element attributes
	 * 
	 * @return the tr
	 */
	public String getTr() {

		return this.tr;
	}

	/**
	 * Return the td element attributes
	 * 
	 * @return the td
	 */
	public String getTd() {

		return this.td;
	}

	@Override
	public String toString() {

		return this.getClass().getCanonicalName();
	}
}