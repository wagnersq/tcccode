package br.com.wagner.tcc.ie.utils;

/**
 * 
 * @author Wagner Queiroz
 *
 * @since 1.0
 */

public final class Nullity {

	/**
	 * Private constructor
	 */
	private Nullity() {
	}

	/**
	 * Test if object is null or neither
	 * 
	 * @param str
	 * @return true or false
	 */
	public static boolean isNull(final String str) {

		return null == str;
	}

	/**
	 * Test if objects if empty or neither
	 * 
	 * @param str
	 * @return true or false
	 */
	public static boolean isEmpty(final String str) {

		return null != str && str.isEmpty();
	}

	@Override
	public String toString() {

		return this.getClass().getCanonicalName();
	}

}