package br.com.wagner.tcc.ie.models;

import br.com.wagner.tcc.ie.enums.Idioms;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public final class IdiomaticExpression {

	private final Idioms from;

	private final Idioms to;

	private final String expression;

	private final String translate;

	/**
	 * Custom constructor
	 * 
	 * @param from
	 * @param to
	 * @param expression
	 * @param translate
	 */
	public IdiomaticExpression(final Idioms from, final Idioms to, final String expression, final String translate) {

		this.from = from;

		this.to = to;

		this.expression = expression;

		this.translate = translate;
	}

	/**
	 * Return from idiom
	 * 
	 * @return the from
	 */
	public Idioms getFrom() {

		return this.from;
	}

	/**
	 * Return to idiom
	 * 
	 * @return the to
	 */
	public Idioms getTo() {

		return this.to;
	}

	/**
	 * Return the expression
	 * 
	 * @return the word
	 */
	public String getExpression() {

		return this.expression;
	}

	/**
	 * Return the translate expression
	 * 
	 * @return the translate
	 */
	public String getTranslate() {

		return this.translate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		result = prime * result + ((translate == null) ? 0 : translate.hashCode());
		result = prime * result + ((expression == null) ? 0 : expression.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object otherObject) {
		if (otherObject == null) {
			return false;
		}
		if (this == otherObject) {
			return true;
		}
		if (!(otherObject instanceof IdiomaticExpression)) {
			return false;
		}
		IdiomaticExpression other = (IdiomaticExpression) otherObject;
		if (from == null) {
			if (other.from != null) {
				return false;
			}
		} else if (!from.equals(other.from)) {
			return false;
		}
		if (to == null) {
			if (other.to != null) {
				return false;
			}
		} else if (!to.equals(other.to)) {
			return false;
		}
		if (translate == null) {
			if (other.translate != null) {
				return false;
			}
		} else if (!translate.equals(other.translate)) {
			return false;
		}
		if (expression == null) {
			if (other.expression != null) {
				return false;
			}
		} else if (!expression.equals(other.expression)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {

		return this.getClass().getCanonicalName();
	}
}