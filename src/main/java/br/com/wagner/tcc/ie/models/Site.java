package br.com.wagner.tcc.ie.models;

import java.net.URL;

import br.com.wagner.tcc.ie.exceptions.InvalidAttributeStateException;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public final class Site {

	private final URL url;

	/**
	 * Custom constructor
	 * 
	 * @param url
	 * @throws InvalidAttributeStateException
	 */
	public Site(final String url) throws InvalidAttributeStateException {

		try {

			this.url = new URL(url);

		} catch (final Exception exception) {

			throw new InvalidAttributeStateException(exception);
		}
	}

	/**
	 * Custom constructor
	 * 
	 * @param url
	 */
	public Site(final URL url) {

		this.url = url;
	}

	/**
	 * Return the URL of site
	 * 
	 * @return url
	 */

	public URL getUrl() {

		return this.url;
	}

	/**
	 * Return the string of URL of site
	 * 
	 * @return url
	 */
	public String getStrURL() {

		return this.getUrl().toString();
	}

	@Override
	public String toString() {

		return this.getClass().getCanonicalName();
	}
}