/**
 * 
 */
package br.com.wagner.tcc.ie.interfaces;

import java.util.List;

import br.com.wagner.tcc.ie.models.IdiomaticExpression;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public interface RecoverStrategy {

	List<IdiomaticExpression> recoverIdiomaticExpressions();
}