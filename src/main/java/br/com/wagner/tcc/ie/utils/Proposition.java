package br.com.wagner.tcc.ie.utils;

import br.com.wagner.tcc.ie.exceptions.InvalidAttributeStateException;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public final class Proposition {

	/**
	 * Check if value is valid otherwise throw exception.
	 * 
	 * @param str
	 * @throws InvalidAttributeStateException
	 */
	public static void checkValue(final String str) throws InvalidAttributeStateException {

		if (verifyNullOrEmpty(str))
			throw new InvalidAttributeStateException();
	}

	/**
	 * Checks null or empty value.
	 * 
	 * @param str
	 * @return true or false
	 */
	private static boolean verifyNullOrEmpty(final String str) {

		return Nullity.isNull(str) || Nullity.isEmpty(str);
	}

	@Override
	public String toString() {

		return this.getClass().getCanonicalName();
	}

}