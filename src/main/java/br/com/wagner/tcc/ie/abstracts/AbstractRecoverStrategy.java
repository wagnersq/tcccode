package br.com.wagner.tcc.ie.abstracts;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.wagner.tcc.ie.enums.Idioms;
import br.com.wagner.tcc.ie.exceptions.SiteConnectionException;
import br.com.wagner.tcc.ie.interfaces.RecoverStrategy;
import br.com.wagner.tcc.ie.models.Site;
import br.com.wagner.tcc.ie.models.Word;

/**
 * @author Wagner Queiroz
 * 
 * @since 1.0
 */
public abstract class AbstractRecoverStrategy implements RecoverStrategy {

	private final Site site;

	private final Word word;

	private final Idioms from;

	private final Idioms to;

	/**
	 * Custom constructor
	 * 
	 * @param site
	 * @param word
	 */
	protected AbstractRecoverStrategy(final Site site, final Word word, final Idioms from, final Idioms to) {

		this.site = site;

		this.word = word;

		this.from = from;

		this.to = to;
	}

	/**
	 * Return the site
	 * 
	 * @return site
	 */
	protected Site getSite() {

		return this.site;
	}

	/**
	 * Return the word
	 * 
	 * @return word
	 */
	protected Word getWord() {

		return this.word;
	}

	/**
	 * 
	 * Return the from idiom
	 * 
	 * @return the from
	 */
	public Idioms getFrom() {

		return this.from;
	}

	/**
	 * Return the to translate idiom
	 * 
	 * @return the to
	 */
	public Idioms getTo() {
		return to;
	}

	/**
	 * Returns the http url connection.
	 * 
	 * @return HttpURLConnection
	 * @throws SiteConnectionException
	 */
	protected HttpURLConnection getHTTPURLConnection() throws SiteConnectionException {

		final URL url = this.site.getUrl();

		HttpURLConnection urlConnection;

		try {

			urlConnection = (HttpURLConnection) url.openConnection();

		} catch (IOException ioe) {

			throw new SiteConnectionException(ioe);
		}

		return urlConnection;
	}

	@Override
	public String toString() {

		return this.getClass().getCanonicalName();
	}

}